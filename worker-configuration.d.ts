// Generated by Wrangler on Sun Jul 14 2024 16:38:07 GMT-0300 (Argentina Standard Time)
// by running `wrangler types`

interface Env {
	KV: KVNamespace;
	/**
	 * I edited this value because wrangler only
	 * generates the types for the current environment
	 * 
	 * A fix for this issue is being discussed here:
	 * https://github.com/cloudflare/workers-sdk/issues/5082
	 */
	ENVIRONMENT: "dev" | "production";
	CLARITY_ID: string;
	DB: D1Database;
	DB: D1Database;
}
