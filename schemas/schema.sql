DROP TABLE IF EXISTS products;
CREATE TABLE IF NOT EXISTS products (
  id integer PRIMARY KEY AUTOINCREMENT,
  name text NOT NULL,
  original_price integer NOT NULL,
  price_in_dollars integer NOT NULL,
  show_original_price boolean NOT NULL,
  photo text NOT NULL,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
);
CREATE INDEX idx_comments_post_slug ON products (id);
