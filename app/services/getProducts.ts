import dayjs from "dayjs";
import { API_URL } from "~/constants";
import type {
  ApiError,
  ApiProduct,
  ApiResponseProducts,
  ProductProps,
} from "~/types";
import getCotization from "./getCotization";
import type { ProductsPaginationData } from "./kv";
import { getPage, savePage } from "./kv";

const productsUrl = `${API_URL}/products`;

/**
 * Replaces imgs urls with my own service who has cache-control header
 * on /images/{id_product}.{png,webp}
 */
function getPhotoWithCache(photo: string) {
  const photoUrl = new URL(photo);
  const photoUrlParts = photoUrl.pathname.split("/");
  const product = photoUrlParts[photoUrlParts.length - 1];

  return `/images/${product}`;
}

function prepareProduct(
  { id, name, price, originalPrice, photo, updatedAt }: ApiProduct,
  cotization: number,
): ProductProps {
  // a recent product is a product which was updated
  // in the last month
  const oneMonthAgo = dayjs().subtract(1, "month");

  return {
    id,
    name,
    price: price.toFixed(2),
    originalPrice: originalPrice.toFixed(2),
    priceInDollars: (price / cotization).toFixed(2),
    showOriginalPrice: originalPrice > price,
    photo: getPhotoWithCache(photo),
    isRecent: dayjs(updatedAt).isAfter(oneMonthAgo),
  };
}

export default async function getProducts(
  KV: KVNamespace<string>,
  page = 1,
): Promise<ProductsPaginationData | ApiError> {
  const cached = await getPage(KV, page);

  if (cached) {
    return cached;
  }

  const response = await fetch(`${productsUrl}?page=${page}`, {
    method: "GET",
  });

  const [data, cotization] = await Promise.all([
    response.json() as Promise<ApiResponseProducts>,
    getCotization(),
  ]);

  if (!data.products) {
    return { status: 404, type: "error" };
  }

  const productsData = {
    page: data.page,
    per_page: data.per_page,
    isLastPage: data.page_count === data.page,
    products: data.products
      .map((product) => prepareProduct(product, cotization))
      .filter((p) => p.isRecent),
    type: "products" as const,
  };

  savePage(KV, page, productsData);

  return productsData;
}

export async function getAllProducts(KV: KVNamespace<string>) {
  let page = 1;
  let products: ProductProps[] = [];

  while (true) {
    const data = await getProducts(KV, page);

    if ("status" in data) {
      throw new Error("Error fetching products");
    }

    products = [...products, ...data.products];

    if (data.isLastPage) {
      break;
    }

    page++;
  }

  return products;
}
