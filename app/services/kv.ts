import type { Cart, ProductProps } from "~/types";
import { getAllProducts } from "./getProducts";

export interface ProductsPaginationData {
  page: number;
  per_page: number;
  isLastPage: boolean;
  products: ProductProps[];
  type: "products";
}

export async function getPage(KV: KVNamespace<string>, page: number) {
  return (await KV.get(
    `page:${page}`,
    "json",
  )) as ProductsPaginationData | null;
}

export async function savePage(
  KV: KVNamespace<string>,
  page: number,
  data: ProductsPaginationData,
) {
  return KV.put(`page:${page}`, JSON.stringify(data), {
    expirationTtl: 3600,
  });
}

export async function getCart(KV: KVNamespace<string>): Promise<Cart> {
  const cart = (await KV.get("cart", "json")) as Cart | undefined | null;

  const products = await getAllProducts(KV);

  if (!cart) {
    return {};
  }

  const cartKeys = Object.keys(cart);
  const productsKeys = new Set(products.map((p) => p.id));

  const invalidKeys = cartKeys.filter((k) => !productsKeys.has(k));

  for (const k of invalidKeys) {
    delete cart[k];
  }

  return cart;
}

async function saveCart(KV: KVNamespace<string>, cart: Cart) {
  return KV.put("cart", JSON.stringify(cart));
}

export async function addProductToCart(KV: KVNamespace<string>, id: string) {
  const products = await getAllProducts(KV);
  const cart = await getCart(KV);

  if (cart[id]) {
    cart[id].quantity = cart[id].quantity + 1;
  } else {
    const product = products.find((p) => p.id === id);
    if (product) {
      cart[id] = { ...product, quantity: 1 };
    }
  }

  return saveCart(KV, cart);
}

export async function removeProductFromCart(
  KV: KVNamespace<string>,
  id: string,
) {
  const cart = await getCart(KV);

  if (cart[id] && cart[id].quantity > 1) {
    cart[id].quantity = cart[id].quantity - 1;
  } else {
    delete cart[id];
  }

  return saveCart(KV, cart);
}
