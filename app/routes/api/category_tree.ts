import type { LoaderFunction } from "@remix-run/cloudflare";
import { json } from "@remix-run/cloudflare";
import type { Category } from "~/services/getCategories";
import getCategories from "~/services/getCategories";

interface ExtendedCategory extends Category {
  subcategories: Category[];
}

export const loader: LoaderFunction = async () => {
  const categoryTree: Record<number, ExtendedCategory> = {};
  const visited = new Map<number, ExtendedCategory>();

  const categories = await getCategories();

  for (const category of categories) {
    const obj = { ...category, subcategories: [] };
    visited.set(obj.id, obj);

    if (obj.parent_id === null) {
      categoryTree[obj.id] = obj;
    } else {
      visited.get(obj.parent_id)?.subcategories.push(obj);
    }
  }

  return json(categoryTree);
};
