import type { LoaderFunction } from "@remix-run/cloudflare";
import { json } from "@remix-run/cloudflare";

import getProducts from "~/services/getProducts";
import type { ApiError } from "~/types";

export const loader: LoaderFunction = async ({ request, context }) => {
  const { KV } = context.cloudflare.env;

  const url = new URL(request.url);
  const page = Number.parseInt(url.searchParams.get("page") ?? "1");
  const products = await getProducts(KV, page);

  if ((products as ApiError).status === 404) {
    return json(
      { status: 404, message: "Products not found" },
      { status: 404 },
    );
  }

  return json(products, {
    headers: {
      "Cache-Control": `public, max-age=60, stale-while-revalidate=${
        3600 - 60
      }`,
    },
  });
};
