import type { ActionFunction, LoaderFunctionArgs } from "@remix-run/cloudflare";
import { defer, json } from "@remix-run/cloudflare";
import { Await, useLoaderData } from "@remix-run/react";

import getProducts from "~/services/getProducts";

import { Suspense } from "react";
import Header from "~/components/Header";
import { ProductSkeleton } from "~/components/Product";
import { ProductGrid, Products } from "~/components/Products";
import {
  addProductToCart,
  getCart,
  removeProductFromCart,
} from "~/services/kv";
import type { Cart } from "~/types";

async function calculateCartTotals(cart: Cart) {
  const totals = {
    total: 0,
    quantity: 0,
  };

  for (const product of Object.values(cart)) {
    totals.total += Number.parseFloat(product.price) * product.quantity;
    totals.quantity += product.quantity;
  }

  return totals;
}

/**
 * Saves how much of each product is the user buying
 */
export const action: ActionFunction = async ({ request, context }) => {
  const { KV } = context.cloudflare.env;

  if (request.method !== "POST") {
    return json(
      { status: 405, message: "Method not allowed" },
      { status: 405 },
    );
  }

  const body = await request.formData();

  const id = body.get("productId");
  const action = body.get("_action");

  if (typeof id !== "string") {
    return json({ status: 400, message: "Bad request" }, { status: 400 });
  }

  if (action === "add") {
    await addProductToCart(KV, id);
    return json({ status: 200, message: "Added to cart" });
  }

  if (action === "remove") {
    await removeProductFromCart(KV, id);
    return json({ status: 200, message: "Removed from cart" });
  }

  return json({ status: 400, message: "Bad request" }, { status: 400 });
};

export async function loader({ request, context }: LoaderFunctionArgs) {
  const { KV } = context.cloudflare.env;

  const url = new URL(request.url);
  const page = Number.parseInt(url.searchParams.get("page") ?? "1");
  const products = getProducts(KV, page);

  const cart = getCart(KV);

  return defer({
    products,
    cart,
    totals: cart.then((resolvedCart) => calculateCartTotals(resolvedCart)),
  });
}

export default function Index() {
  const data = useLoaderData<typeof loader>();

  return (
    <>
      <Header />
      <main className="flex w-full flex-col gap-2 p-4">
        <h1 className="my-4 font-title text-2xl">Almacén</h1>

        <ProductGrid>
          <Suspense
            fallback={Array.from({ length: 16 }, (_, index) => (
              // biome-ignore lint/suspicious/noArrayIndexKey:
              <ProductSkeleton key={index} />
            ))}
          >
            <Await resolve={data.products}>
              {(products) =>
                products.type === "error" ? (
                  products.status === 404 ? (
                    <p className="text-red-500">No products found</p>
                  ) : (
                    <p className="text-red-500">An error occurred</p>
                  )
                ) : (
                  <Products
                    products={products.products}
                    page={products.page}
                    isLastPage={products.isLastPage}
                  />
                )
              }
            </Await>
          </Suspense>
        </ProductGrid>
      </main>
    </>
  );
}
