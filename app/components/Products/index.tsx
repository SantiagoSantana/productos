import { startTransition, useCallback, useReducer, useRef } from "react";

import type { ReactElement } from "react";

import Product from "~/components/Product";
import { useIntersect } from "~/hooks/useIntersect";
import type { ProductsPaginationData } from "~/services/kv";
import type { ApiError, ProductProps } from "~/types";

export function ProductGrid({ children }: { children: ReactElement }) {
  return (
    <div className="grid grid-cols-[repeat(auto-fill,minmax(160px,auto))] gap-x-4 gap-y-3">
      {children}
    </div>
  );
}

interface ProductsData {
  pagesLoaded: Set<number>;
  page: number;
  products: ProductProps[];
  isLastPage: boolean;
}

type ActionAddPage = {
  type: "ADD_PAGE";
  payload: Omit<ProductsData, "pagesLoaded">;
};

type Action = ActionAddPage;

function reducer(state: ProductsData, action: Action): ProductsData {
  switch (action.type) {
    case "ADD_PAGE":
      if (state.pagesLoaded.has(action.payload.page)) {
        return state;
      }

      return {
        page: action.payload.page,
        isLastPage: action.payload.isLastPage,
        products: [...state.products, ...action.payload.products],
        pagesLoaded: new Set([...state.pagesLoaded, action.payload.page]),
      };
  }
}

export function Products({
  page,
  products,
  isLastPage,
}: Omit<ProductsData, "pagesLoaded">): ReactElement {
  const [data, dispatch] = useReducer(reducer, {
    page,
    products,
    isLastPage,
    pagesLoaded: new Set([page]),
  });

  /**
   * It's safe to use a ref here because it's not read or write
   * during render
   */
  const alreadyFetchedPages = useRef<number[]>([page]);
  const intersectProductElement = useRef<HTMLDivElement | null>(null);

  const callback = useCallback<IntersectionObserverCallback>(
    (entries) => {
      for (const entry of entries) {
        if (
          entry.isIntersecting &&
          !data.isLastPage &&
          !alreadyFetchedPages.current.includes(data.page + 1)
        ) {
          alreadyFetchedPages.current.push(data.page + 1);

          startTransition(async () => {
            console.log("fetching next page", data.page + 1);
            const searchParams = new URLSearchParams({
              page: `${data.page + 1}`,
            });

            const response = await fetch(`/api?${searchParams}`);

            if (!response.ok) {
              return;
            }

            const responseData = (await response.json()) as
              | ProductsPaginationData
              | ApiError;

            if ("status" in responseData) {
              return;
            }

            console.log("dispatching ADD_PAGE", responseData);
            dispatch({ type: "ADD_PAGE", payload: responseData });
          });
        }
      }
    },
    [data],
  );

  useIntersect(callback, intersectProductElement);

  const totalProducts = data.products.length;

  // if 60% of elements are past the viewport, load more
  const intersectProductIndex = Math.floor(totalProducts * 0.6);

  return (
    <>
      {data.products.map(
        (
          { id, photo, name, showOriginalPrice, price, originalPrice },
          index,
        ) => (
          <div
            key={id}
            ref={(node) => {
              if (index === intersectProductIndex) {
                intersectProductElement.current = node;
              }
            }}
          >
            <Product
              id={id}
              photo={photo}
              name={name}
              showOriginalPrice={showOriginalPrice}
              price={price}
              originalPrice={originalPrice}
            />
          </div>
        ),
      )}
    </>
  );
}
