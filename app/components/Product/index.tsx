import { Await, useLoaderData } from "@remix-run/react";
import { type ReactElement, Suspense } from "react";
import type { loader } from "~/routes";
import type { ProductProps } from "~/types";
import { ProductActions } from "../ProductActions";

function withExtension(photo: string, ext: string) {
  return photo.replace(/\.jpg/, `.${ext}`);
}

export function ProductSkeleton() {
  return (
    <div className=" flex h-[300px] animate-pulse flex-col items-center justify-center rounded-sm bg-white px-3 py-4 text-center align-center text-sm shadow-md" />
  );
}

export default function Index({
  id,
  photo,
  name,
  showOriginalPrice,
  originalPrice,
  price,
}: Omit<ProductProps, "priceInDollars" | "isRecent">): ReactElement {
  const data = useLoaderData<typeof loader>();

  return (
    <div className="flex h-[300px] flex-col items-center justify-center rounded-sm bg-white px-3 py-4 text-center align-center text-sm shadow-md">
      <picture>
        <source srcSet={withExtension(photo, "webp")} type="image/webp" />
        <img
          className="size-[148px] max-w-none"
          width="148"
          height="148"
          src={photo}
          alt=""
        />
      </picture>

      <div className="flex flex-col items-center justify-center gap-3">
        <p className="line-clamp-2 h-10 min-h-10 overflow-ellipsis">{name}</p>
        <p className="flex h-full items-center gap-[6px] align-middle font-semibold">
          {showOriginalPrice ? (
            <span className="text-xs line-through">${originalPrice}</span>
          ) : null}
          <span className="text-accent text-base">${price}</span>
        </p>
        <Suspense>
          <Await resolve={data.cart}>
            {() => {
              return <ProductActions id={id} price={price} />;
            }}
          </Await>
        </Suspense>
      </div>
    </div>
  );
}
