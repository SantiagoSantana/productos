import { Await, useLoaderData } from "@remix-run/react";
import { Suspense } from "react";
import type { loader } from "~/routes";
import { ProductActions } from "../ProductActions";

export function Cart() {
  const data = useLoaderData<typeof loader>();

  return (
    <Suspense>
      <Await resolve={data.cart}>
        {(cart) => (
          <div className="flex flex-col items-center justify-center bg-white">
            <ul className="list-none">
              {Object.entries(cart).map(([id, product]) => {
                const subtotal =
                  product.quantity * Number.parseFloat(product.price);
                return (
                  <li
                    className="flex items-center justify-between gap-3 border-gray-200 border-b border-solid p-3"
                    key={id}
                  >
                    <img
                      className="size-20"
                      width="80"
                      height="80"
                      src={product.photo}
                      alt=""
                    />
                    <div className="flex flex-col items-start justify-center font-semibold text-sm">
                      <h2 className="line-clamp-2 max-w-[20ch] overflow-ellipsis">
                        {product.name}
                      </h2>
                      <p>${subtotal.toFixed(2)}</p>
                    </div>
                    <ProductActions id={id} price={subtotal.toFixed(2)} />
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </Await>
    </Suspense>
  );
}
