import type { SerializeFrom } from "@remix-run/cloudflare";
import {
  type FetcherWithComponents,
  useAsyncValue,
  useFetcher,
} from "@remix-run/react";
import type { ReactElement } from "react";
import { useMemo } from "react";
import type { loader } from "~/routes";
import type { Cart } from "~/types";

function Form({
  fetcher,
  isSubmitting,
  children,
}: {
  fetcher: FetcherWithComponents<SerializeFrom<typeof loader>>;
  isSubmitting: boolean;
  children: React.ReactNode;
}) {
  return (
    <fetcher.Form
      className={`flex w-[132px] justify-between ${isSubmitting && "opacity-50"}`}
      method="post"
    >
      {children}
    </fetcher.Form>
  );
}

function ModifyQuantityButton({
  type,
  isSubmitting,
}: {
  type: "add" | "remove";
  isSubmitting: boolean;
}) {
  return (
    <button
      type="submit"
      className="flex size-8 items-center justify-center rounded bg-accent text-white"
      name="_action"
      value={type}
      disabled={isSubmitting}
    >
      {type === "add" ? "+" : "−"}
    </button>
  );
}

export function ProductActions({
  id,
}: {
  id: string;
  price: string;
}): ReactElement {
  const cart = useAsyncValue() as Cart;

  const fetcher = useFetcher<typeof loader>();

  const isSubmitting = fetcher.formData?.get("productId") === id;

  const optimisticQuantity = useMemo(() => {
    const dataQuantity = cart[id]?.quantity ?? 0;

    if (isSubmitting) {
      if (fetcher.formData?.get("_action") === "add") {
        return dataQuantity + 1;
      }

      if (fetcher.formData?.get("_action") === "remove") {
        return dataQuantity - 1;
      }
    }

    return dataQuantity;
  }, [fetcher.formData, cart, id, isSubmitting]);

  if (optimisticQuantity === 0) {
    return (
      <Form isSubmitting={isSubmitting} fetcher={fetcher}>
        <input type="hidden" name="productId" value={id} />
        <button
          type="submit"
          className="h-8 w-[132px] rounded-sm border border-accent border-solid bg-white text-[13.33px] text-accent"
          name="_action"
          value="add"
          disabled={isSubmitting}
          style={{ "--opacity": isSubmitting ? 0.5 : 1 }}
        >
          {isSubmitting ? "Agregando..." : "Agregar al carrito"}
        </button>
      </Form>
    );
  }

  return (
    <Form isSubmitting={isSubmitting} fetcher={fetcher}>
      <input type="hidden" name="productId" value={id} />
      <ModifyQuantityButton type="remove" isSubmitting={isSubmitting} />
      <span className="flex h-8 w-[52px] justify-center font-semibold text-accent text-sm leading-8">
        {optimisticQuantity}
      </span>
      <ModifyQuantityButton type="add" isSubmitting={isSubmitting} />
    </Form>
  );
}
