import * as Popover from "@radix-ui/react-popover";
import { Await, useLoaderData } from "@remix-run/react";
import { Suspense } from "react";
import type { loader } from "~/routes";
import { Cart } from "../Cart";
import shoppingCart from "./cart.png";
import logo from "./logo.png";

export default function Header() {
  const data = useLoaderData<typeof loader>();

  return (
    <header className="flex min-h-[62px] w-full items-center justify-between bg-header-background p-4 text-header-foreground shadow-md">
      <div className="flex h-full items-center gap-[0.6rem] text-[17px] leading-[25.5px]">
        <img
          className="-tracking-[0.41px] size-[30px]"
          width="30"
          height="30"
          src={logo}
          alt="logo"
        />
        <span>
          <b className="font-semibold">Ez</b>
          <span className="font-normal">shop</span>
        </span>
      </div>
      <div className="flex items-center text-sm">
        <Suspense>
          <Await resolve={data.totals}>
            {(totals) => (
              <strong className="font-semibold">
                ${totals.total.toFixed(2) ?? 0}
              </strong>
            )}
          </Await>
        </Suspense>
        <Popover.Root>
          <Popover.Trigger className="px-1">
            <div className="relative ml-[0.6em] flex size-[22px] justify-end">
              <img
                className="size-[22px]"
                src={shoppingCart}
                alt="shopping cart"
                width="22px"
                height="22px"
              />
              <span className="-top-2 -right-[10px] absolute inline-flex size-3 items-center justify-center rounded-full border-2 border-white border-solid bg-accent p-2 font-bold text-[8px] text-white">
                <Suspense>
                  <Await resolve={data.totals}>
                    {(totals) => (
                      <strong className="font-semibold">
                        {totals.quantity ?? 0}
                      </strong>
                    )}
                  </Await>
                </Suspense>
              </span>
            </div>
          </Popover.Trigger>
          <Popover.Portal>
            <Popover.Content>
              <Cart />
            </Popover.Content>
          </Popover.Portal>
        </Popover.Root>
      </div>
    </header>
  );
}
