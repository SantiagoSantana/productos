export interface ApiProduct {
  id: string;
  name: string;
  price: number;
  presentation: string;
  brand: string;
  photo: string;
  originalPrice: number;
  updatedAt: string;
}

export interface ApiResponseProducts {
  products: ApiProduct[];
  page: number;
  per_page: number;
  page_count: number;
}

export interface ProductProps {
  id: string;
  name: string;
  price: string;
  originalPrice: string;
  priceInDollars: string;
  showOriginalPrice: boolean;
  photo: string;
  isRecent: boolean;
}

export interface ApiError {
  status: number;
  message?: string;
  type: "error";
}

export type Cart = {
  [key: string]: ProductProps & { quantity: number };
};
