import { useEffect } from "react";

export function useIntersect(
  callback: IntersectionObserverCallback,
  ref: React.RefObject<HTMLElement | null>,
) {
  useEffect(() => {
    if (!ref.current) {
      return;
    }

    console.log("useIntersect");

    const options: IntersectionObserverInit = {
      root: null,
      rootMargin: "0px",
      threshold: 0,
    };

    const observer = new IntersectionObserver(callback, options);

    observer.observe(ref.current as HTMLElement);

    return () => {
      observer.disconnect();
    };
  }, [ref.current, callback]);
}
