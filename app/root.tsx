import type { LinksFunction, LoaderFunctionArgs } from "@remix-run/cloudflare";
import {
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  json,
  useLoaderData,
} from "@remix-run/react";
import type React from "react";

import "~/styles/global.css";

// https://remix.run/api/app#links
export const links: LinksFunction = () => {
  return [
    {
      rel: "preload",
      as: "font",
      href: "/fonts/sf-pro-text-regular.woff",
      type: "font/woff",
      crossOrigin: "anonymous",
    },
    {
      rel: "preload",
      as: "font",
      href: "/fonts/sf-pro-text-semibold.woff",
      type: "font/woff",
      crossOrigin: "anonymous",
    },
    {
      rel: "preload",
      as: "font",
      href: "/fonts/sf-pro-text-medium.woff",
      type: "font/woff",
      crossOrigin: "anonymous",
    },
    {
      rel: "preload",
      as: "font",
      href: "/fonts/sf-pro-text-bold.woff",
      type: "font/woff",
      crossOrigin: "anonymous",
    },
  ];
};

export function Layout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body className="min-h-screen bg-background font-sans text-black leading-6">
        {children}
        <ScrollRestoration />
        <Scripts />
      </body>
    </html>
  );
}

export const loader = async ({ context }: LoaderFunctionArgs) => {
  return json({
    environment: context.cloudflare.env.ENVIRONMENT,
    clarityId: context.cloudflare.env.CLARITY_ID,
  });
};

/**
 * Do not revalidate this route ever.
 *
 * Be sure that the data for this route is static and will not change
 * when modifying this loader.
 */
export const shouldRevalidate = () => false;

export default function App() {
  const data = useLoaderData<typeof loader>();

  return (
    <>
      {data.environment === "production" && (
        <script
          type="text/javascript"
          async
          /**
           * biome-ignore lint/security/noDangerouslySetInnerHtml: the data for this script comes
           * from a cloudflare environment variable, so it's safe to use
           */
          dangerouslySetInnerHTML={{
            __html: `
              (function(c,l,a,r,i,t,y){
                  c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
                  t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
                  y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
              })(window, document, "clarity", "script", "${data.clarityId}");
          `,
          }}
        />
      )}
      <Outlet />
    </>
  );
}
