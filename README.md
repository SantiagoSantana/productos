# Challenge

The challenge is to build a product grid that fetches data from an API and allows the user to add products to a cart.

The application was developed with the [remix](https://remix.run/) framework running on [Cloudflare Workers](https://workers.cloudflare.com/)

- [site](https://remix-cloudflare-workers.santiagojs.workers.dev)
- [api](https://remix-cloudflare-workers.santiagojs.workers.dev/api)
- [category tree](https://remix-cloudflare-workers.santiagojs.workers.dev/api/category_tree)

## Installation

```sh
git clone https://gitlab.com/SantiagoSantana/productos
cd productos
pnpm install
pnpm run dev
```

Una vez ejecutados los comandos anteriores el servicio debería estar disponible en [localhost:8787](localhost:8787)

## Manual Deployment

[wrangler](https://developers.cloudflare.com/workers/cli-wrangler) is required to build and deploy the application to Cloudflare Workers.

Instructions for installing it can be found in [the installation guide](https://developers.cloudflare.com/workers/cli-wrangler/install-update).

It is also necessary to [authenticate the CLI with cloudflare](https://developers.cloudflare.com/workers/wrangler/commands/#login).

Once this is done, you can run the following command to deploy

```sh
npm run deploy
```

Every push to the repository triggers a deploy in a [Gitlab CI pipeline](./.gitlab-ci.yml).

## Features

When accessing the main route `/`, the product grid can be seen. The data that provides this grid can be seen in the path `/api` (this path receive a page query param that is used to paginate the products).

When accessing the route `/api/category_tree` the category tree can be obtained.

- The products load in an infinite scrolling.
- The buttons to add and subtract products to the cart work. Each operation is saved on [KV](https://developers.cloudflare.com/kv/) and recovered when the page is reloaded.
- The app only fetches the most recent products (those that have been updated in the last month) and the price in dollars for each product.
- The site is automatically deployed to Cloudflare Workers on each push, `biome`, `knip` and `tsc` are also run on each pipeline execution prior to deployment. The installation of dependencies is also cached in gitlab to speed up the pipeline.
- `webp` is used when available (picture with srcset). The `Cache-Control` header is added to the imgs to prevent the browser from loading them every time the page is accessed. See [the images api](./app/routes/images/$productId.tsx)
- Products are fetched from https://challenge-api.aerolab.co/products and cached in KV for an hour.
- There was an attempt to cache the products in the CDN using the `Cache-Control` header, but since the Cloudflare Workers are before the CDN (a request to a worker does not reach the CDN) the header is only respected by the browser.
  - It is possible to cache from the cloudflare workers using the Cache API but it is not available in `*.workers.dev` domains ([all operations on the cache in these subdomains are ignored](https://developers.cloudflare.com/workers/runtime-apis/cache))
- The route `/api/category_tree` brings the category tree.

## Linting

The `lint` script runs [biome](https://biomejs.dev/), and `tsc` to lint, format and type-check the code.

```sh
pnpm lint
```

There is a pre-commit hook that runs the `lint` script over staged files before committing. It also runs [knip](https://knip.dev/) to avoid commiting unused files, dependencies or exports

It runs the two scripts in parallel, to speed up the process.

All of the above is also run in the Gitlab CI pipeline over all files before deploying, so the pre-commit hook can be skipped if desired.

## Structure of the app

All the source code that is not related to the framework or the cloudflare workers is in the `app` folder.

- `app`
  - `components`: react components used in the routes
  - `routes`: app routes, match with pathnames (I'm using the v1 route conventions)
  - `services`: main logic of the application, routes request data from services
  - `constants.tsx`: constants used throughout the app
