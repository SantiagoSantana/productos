/** @type {import('tailwindcss').Config} */
export default {
  content: ["./app/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["var(--font-body)"],
        title: ["var(--font-title)"],
      },
      colors: {
        background: "var(--color-background)",
        "header-background": "var(--header-background)",
        "header-foreground": "var(--header-foreground)",
        accent: "var(--color-accent)",
      },
    },
  },
  plugins: [],
};
